package com.fcolasuonno.romanNumeralGenerator.runner;

import com.fcolasuonno.romanNumeralGenerator.RomanNumeralGenerator;
import com.fcolasuonno.romanNumeralGenerator.RomanNumeralGeneratorFactory;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Helper class to allow using the number generator from command line using stdin/stdout
 */
class Runner {

    public static void main(String[] args) {
        boolean optimized = true;
        if (args.length > 0 && "--simple".equalsIgnoreCase(args[0])) {
            optimized = false;
        }
        if (args.length > 0 && "--opt".equalsIgnoreCase(args[0])) {
            optimized = true;
        }
        if (args.length > 0 && "--help".equalsIgnoreCase(args[0])) {
            System.out.println("Converts numbers to roman numerals");
            System.out.println("options:");
            System.out.println(" --simple   simple implementation");
            System.out.println(" --opt      optimized implementation");
            System.out.println(" --help     displays this help");
            System.exit(0);
        }
        RomanNumeralGenerator generator = new RomanNumeralGeneratorFactory().create(optimized);
        String romanNumber;
        int num = 1;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter 0 to quit");
        while (num > 0) {
            try {
                num = in.nextInt();
                romanNumber = generator.generate(num);
                System.out.println(romanNumber);
            } catch (InputMismatchException e) {
                num = 0;
            }
        }
    }
}

package com.fcolasuonno.romanNumeralGenerator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Acceptance tests for bounds
 */
public abstract class RomanNumeralGeneratorImplTestBounds {

    protected RomanNumeralGenerator generator;

    @Before
    public abstract void setUp();

    @Test
    public void testLowerBound() throws Exception {
        assertNotNull("1 should generate a valid roman numeral", generator.generate(1));
        assertNull("0 is not a number in the valid range", generator.generate(0));
    }

    @Test
    public void testUpperBound() throws Exception {
        assertNotNull("3999 should generate a valid roman numeral", generator.generate(3999));
        assertNull("4000 is not a number in the valid range", generator.generate(4000));
        assertNull("23000 is not a number in the valid range", generator.generate(23000));
        assertNull("MAX VALUE is not a number in the valid range", generator.generate(Integer.MAX_VALUE));
    }

    @Test
    public void testNegativeNumbers() throws Exception {
        assertNull("-1 is negative, so it should not be accepted", generator.generate(-1));
        assertNull("-10 is negative, so it should not be accepted", generator.generate(-10));
        assertNull("-42 is negative, so it should not be accepted", generator.generate(-42));
        assertNull("-20000 is negative, so it should not be accepted", generator.generate(-20000));
        assertNull("MIN VALUE is negative, so it should not be accepted", generator.generate(Integer.MIN_VALUE));
    }

}
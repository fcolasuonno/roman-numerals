package com.fcolasuonno.romanNumeralGenerator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test correctness of different number buckets
 */
@SuppressWarnings("SpellCheckingInspection")
public abstract class RomanNumeralGeneratorImplTestNumbers {

    protected RomanNumeralGenerator generator;

    @Before
    public abstract void setUp();

    private void checkRomanNumber(int number, String representation) {
        assertEquals(number + " should be equal to " + representation, representation, generator.generate(number));
    }

    @Test
    public void testUnits() throws Exception {
        checkRomanNumber(1, "I");
        checkRomanNumber(2, "II");
        checkRomanNumber(3, "III");
        checkRomanNumber(4, "IV");
        checkRomanNumber(5, "V");
        checkRomanNumber(6, "VI");
        checkRomanNumber(7, "VII");
        checkRomanNumber(8, "VIII");
        checkRomanNumber(9, "IX");
    }

    @Test
    public void testTens() throws Exception {
        checkRomanNumber(10, "X");
        checkRomanNumber(11, "XI");
        checkRomanNumber(12, "XII");
        checkRomanNumber(13, "XIII");
        checkRomanNumber(14, "XIV");
        checkRomanNumber(15, "XV");
        checkRomanNumber(16, "XVI");
        checkRomanNumber(17, "XVII");
        checkRomanNumber(18, "XVIII");
        checkRomanNumber(19, "XIX");
        checkRomanNumber(20, "XX");
        checkRomanNumber(21, "XXI");
        checkRomanNumber(22, "XXII");
        checkRomanNumber(25, "XXV");
        checkRomanNumber(26, "XXVI");
        checkRomanNumber(27, "XXVII");
        checkRomanNumber(28, "XXVIII");
        checkRomanNumber(29, "XXIX");
        checkRomanNumber(30, "XXX");
        checkRomanNumber(31, "XXXI");
        checkRomanNumber(32, "XXXII");
        checkRomanNumber(33, "XXXIII");
        checkRomanNumber(40, "XL");
        checkRomanNumber(45, "XLV");
        checkRomanNumber(50, "L");
        checkRomanNumber(60, "LX");
        checkRomanNumber(70, "LXX");
        checkRomanNumber(80, "LXXX");
        checkRomanNumber(90, "XC");
        checkRomanNumber(91, "XCI");
        checkRomanNumber(99, "XCIX");

    }

    @Test
    public void testHundreds() throws Exception {
        checkRomanNumber(100, "C");
        checkRomanNumber(101, "CI");
        checkRomanNumber(102, "CII");
        checkRomanNumber(110, "CX");
        checkRomanNumber(111, "CXI");
        checkRomanNumber(122, "CXXII");
        checkRomanNumber(130, "CXXX");
        checkRomanNumber(135, "CXXXV");
        checkRomanNumber(200, "CC");
        checkRomanNumber(300, "CCC");
        checkRomanNumber(400, "CD");
        checkRomanNumber(500, "D");
        checkRomanNumber(600, "DC");
        checkRomanNumber(700, "DCC");
        checkRomanNumber(800, "DCCC");
        checkRomanNumber(900, "CM");
        checkRomanNumber(999, "CMXCIX");
    }

    @Test
    public void testThousands() throws Exception {
        checkRomanNumber(1000, "M");
        checkRomanNumber(1004, "MIV");
        checkRomanNumber(1060, "MLX");
        checkRomanNumber(1700, "MDCC");
        checkRomanNumber(2300, "MMCCC");
        checkRomanNumber(2999, "MMCMXCIX");
        checkRomanNumber(3000, "MMM");
        checkRomanNumber(3500, "MMMD");
        checkRomanNumber(3999, "MMMCMXCIX");
    }

    @Test
    public void testOthers() throws Exception {
//        checkRomanNumber(4400, "MMMMCD");
//        checkRomanNumber(5000, "V̅");
//        checkRomanNumber(5442, "V̅CDXLII");
//        checkRomanNumber(7432, "V̅MMCDXXXII");
//        checkRomanNumber(9999, "V̅MMMMCMXCIX");
    }

}
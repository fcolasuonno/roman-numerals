package com.fcolasuonno.romanNumeralGenerator;

/**
 * Created by francesco on 28/10/2016.
 */
public class RomanNumeralGeneratorImplTestNumbersImplSimple extends RomanNumeralGeneratorImplTestNumbers {
    @Override
    public void setUp() {
        generator = new RomanNumeralGeneratorFactory().create(false);
    }
}

package com.fcolasuonno.romanNumeralGenerator;

/**
 * Created by francesco on 28/10/2016.
 */
public class RomanNumeralGeneratorImplTestBoundsImplSimple extends RomanNumeralGeneratorImplTestBounds {
    @Override
    public void setUp() {
        generator = new RomanNumeralGeneratorFactory().create(false);
    }
}

package com.fcolasuonno.romanNumeralGenerator;

/**
 * Class to generate a roman numeral from a given number
 * <p>
 * This implementation only accepts numbers in the range 1-3999 INCLUSIVE
 */
class RomanNumeralGeneratorImplSimple implements RomanNumeralGenerator {

    private static final int MIN_VALUE = 1;
    private static final int MAX_VALUE = 3999; //9999

    @SuppressWarnings("SpellCheckingInspection")
    private static final String[][] positionalDigitTranslationTable = {
            {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, //units
            {"X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, //tens
            {"C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, //hundreds
            {"M", "MM", "MMM", "MMMM", "V̅", "V̅M", "V̅MM", "V̅MMM", "V̅MMMM"} //thousands - unicode is fun
    };

    @Override
    public String generate(int number) {
        if (number < MIN_VALUE) {
            System.err.println("Number " + number + " is too low, min is " + MIN_VALUE);
            return null;
        }
        if (number > MAX_VALUE) {
            System.err.println("Number " + number + " is too high, max is " + MAX_VALUE);
            return null;
        }

        //each digit requires at most 4 characters, and the highest number has 4 digits, so use 16 as initial capacity
        StringBuilder builder = new StringBuilder(16);
        //we can use stringBuilder since we don't need synchronization

        int powersCounter = 0;
        while (number > 0) {
            //extract the number of units of the current power of 10
            int units = number % 10;

            //romans didn't know about zero, so we skip if the number of units is 0
            if (units > 0) {
                //insert at the front since we are building the string by increasing the power
                builder.insert(0, positionalDigitTranslationTable[powersCounter][units - 1]);
            }

            //prepare for the next iteration, increase the power index
            powersCounter++;
            // and shift the number by one digit
            number /= 10;
        }
        return builder.toString();
    }
}

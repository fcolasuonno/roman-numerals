package com.fcolasuonno.romanNumeralGenerator;

/**
 * Class to generate a roman numeral from a given number using optimization
 * <p>
 * This implementation only accepts numbers in the range 1-3999 INCLUSIVE
 */
class RomanNumeralGeneratorImplOptimized implements RomanNumeralGenerator {
    private static final int MIN_VALUE = 1;
    private static final int MAX_VALUE = 3999;

    private static final char[] symbolTable = {'I', 'V', 'X', 'L', 'C', 'D', 'M'}; //those are the symbols needed to generate numbers from 1 to 3999


    @Override
    public String generate(int number) {
        if (number < MIN_VALUE) {
            System.err.println("Number " + number + " is too low, min is " + MIN_VALUE);
            return null;
        }
        if (number > MAX_VALUE) {
            System.err.println("Number " + number + " is too high, max is " + MAX_VALUE);
            return null;
        }

        //the longest number is 3888 requiring 3+4+4+4 characters, plus a termination char, so 16
        char[] output = new char[16];
        //pointer for the output string buffer
        int outputIndex = 0;

        //we start with the symbol with the symbol with greatest value and iteratively subtract the value from our number
        int symbolIndex = symbolTable.length - 1;
        //stores the power of 10 that is smaller than our symbol value. e.g for 100 it's 10, for 50 it's 10
        int smallerSymbolValue = 1000;

        //iteratively subtract values and emit symbols until we don't have any more units left
        //checking for smallerSymbolValue is redundant, it can never be 0 since we can always subtract 1s from any positive number
        while (number > 0 && smallerSymbolValue > 0) {
            //flag indicating whether the current symbol is a power of ten or not
            boolean isPowerOfTen = symbolIndex % 2 == 0;

            //get a symbol from the symbol table and decrease the index
            char currentSymbol = symbolTable[symbolIndex--];

            //the value of the current symbol depends on then previous iteration and on whether this symbol is a power of 10 or not
            int currentSymbolValue = isPowerOfTen ? smallerSymbolValue : 5 * smallerSymbolValue;
            if (isPowerOfTen) {
                smallerSymbolValue /= 10;
            }
            //only now smallerSymbolValue has the correct value for the given symbol
            // this happens because we can always divide by 10, but we cannot multiply 0 to have a value of 1 for I

            //we emit the symbol if the number is greater than the symbol value, this is effectively a division
            while (number >= currentSymbolValue) {
                output[outputIndex++] = currentSymbol;
                //we are guaranteed to exit the loop eventually because the number is decreasing monotonically
                number -= currentSymbolValue;
            }

            //handle cases for subtractive notation: if the number is closer to our current value then emit special sequence
            if (number >= currentSymbolValue - smallerSymbolValue) {
                //we emit _lower_symbol_ _current_symbol_ in that order, e.g. IX
                //the symbol index associated with the smallerSymbolValue varies according to the isPowerOfTen flag
                output[outputIndex++] = symbolTable[isPowerOfTen ? symbolIndex - 1 : symbolIndex];
                output[outputIndex++] = currentSymbol;
                //since we used subtractive notation we have to fix our number accordingly
                number -= (currentSymbolValue - smallerSymbolValue);
            }
        }

        //we create a string from the buffer using the substring (0, lastWrittenIndex)
        return new String(output, 0, outputIndex);
    }
}

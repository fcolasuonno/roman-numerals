package com.fcolasuonno.romanNumeralGenerator;

/**
 * Interface definition for the problem
 * <p>
 * For an in-depth description of Roman Numerals, see http://en.wikipedia.org/wiki/Roman_numerals
 */
@SuppressWarnings("WeakerAccess")
public interface RomanNumeralGenerator {

    /**
     * Generate a String corresponding to the given number in Roman Numerals
     * <p>
     * LIMITS: Only supports numbers between 1 and 3999
     *
     * @param number the number to convert
     * @return the string representing the number using roman numerals
     */
    String generate(int number);
}

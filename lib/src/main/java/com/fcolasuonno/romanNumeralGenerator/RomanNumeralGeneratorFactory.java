package com.fcolasuonno.romanNumeralGenerator;

/**
 * Simple factory class for implementation of roman numeral generator
 */
public class RomanNumeralGeneratorFactory {
    /**
     * Create a RomanNumeralGenerator
     *
     * @param optimized class implementation to use, optimized is faster, but more complex, not optimized is simpler and more extensible
     * @return an implementation of the RomanNumeralGenerator interface
     */
    public RomanNumeralGenerator create(boolean optimized) {
        return optimized ? new RomanNumeralGeneratorImplOptimized() : new RomanNumeralGeneratorImplSimple();
    }
}
